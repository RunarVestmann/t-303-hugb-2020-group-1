import asyncio
import json
import websockets

import sys
sys.path.append(sys.path[0] + "/..")

from app.system_interface import SystemInterface

class Server:
    """The websocket server that allows clients to connect to our system"""
    
    def __init__(self, interface):
        self.__interface = interface

    async def __handle_message(self, websocket, path):
        """Handles a message from a client and sends a response accordingly"""

        # Wait until a request is received
        request = await websocket.recv()

        # Parse a message from the request and make sure it's in JSON format
        try: 
            message = json.loads(request)
        except json.JSONDecodeError:
            return await websocket.send(json.dumps({"msg": "Invalid JSON format"}))
        except:
            return await websocket.send(json.dumps({"msg": "Error occurred while parsing JSON"}))

        if "op" in message:
            method_name = message["op"]

            if not isinstance(method_name, str): 
                return await websocket.send(json.dumps({"msg": "op field must be a string"}))

            method = getattr(self.__interface, method_name, None)

            if method == None: 
                return await websocket.send(json.dumps({"msg": f"Method {method_name} is not supported"}))

            # Check for any method arguments
            kwargs = {}
            if "data" in message: 
                kwargs = message["data"]

            try:
                # Try calling the method with the given arguments
                await websocket.send(method(**kwargs))
            except TypeError:  
                await websocket.send(json.dumps({"msg": f"Method {method_name} was called with invalid arguments"}))
            except:
                await websocket.send(json.dumps({"msg": f"Error occured while calling method {method_name}"}))
        else: 
            await websocket.send(json.dumps({"msg": "No method was specified"}))

    def start(self, port: int = 8080):
        """Makes the server start listening for messages on the given port (default 8080)"""

        print(f"Server is listening on port {port}")
        asyncio.get_event_loop().run_until_complete(websockets.serve(self.__handle_message, "0.0.0.0", port))
        asyncio.get_event_loop().run_forever()

if __name__ == "__main__":
    server = Server(SystemInterface())
    server.start()
