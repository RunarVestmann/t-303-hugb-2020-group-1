from app.models.entities.poll import Poll
from app.models.entities.poll_data import PollData
from app.models.entities.region_poll import RegionPoll
from app.models.input_models.poll_input_model import PollInputModel
from app.models.input_models.poll_data_input_model import PollDataInputModel
from app.repositories.poll_repository import PollRepository
from app.repositories.election_repository import ElectionRepository
from app.repositories.region_repository import RegionRepository
from app.repositories.candidate_repository import CandidateRepository
import json

class PollService:
    def __init__(self, poll_repository: PollRepository, election_repository: ElectionRepository, region_repository: RegionRepository, candidate_repository: CandidateRepository):
        self.__poll_repository = poll_repository
        self.__election_repository = election_repository
        self.__region_repository = region_repository
        self.__candidate_repository = candidate_repository
    
    def get_all_polls(self) -> [Poll]:
        """Returns a list of the polls in the system"""
        return self.__poll_repository.get_all_polls()

    def get_poll_by_id(self, id: str) -> Poll:
        """Returns a poll by id, if the id doesn't exist, returns an error string"""
        return self.__poll_repository.get_poll_by_id(id)

    def get_polls_by_election_id(self, election_id: str) -> [Poll]:
        """Returns all the polls in the election with the specified id, if the id doesn't exist, returns an error string"""
        return self.__poll_repository.get_polls_by_election_id(election_id)

    def get_poll_summary_by_election_id(self, election_id: str) -> list:
        """Returns a summary of all the polls for a specified election, \n
           i.e. returns how many votes each candidate in an election has in all the polls in the system"""
        election = self.__election_repository.get_election_by_id(election_id)
        if isinstance(election, str):
            return election

        summary = []
        # Add candidates to a list with 0 votes by default
        for candidate in election.candidates:
            summary.append(PollData(candidate, 0))

        polls = self.get_polls_by_election_id(election_id)

        # Iterate through all the polls for all candidates and add the votes to the correct candidate
        for poll in polls:
            for region_poll in poll.region_polls:
                for poll_data in region_poll.data:
                    for item in summary:
                        if item.candidate.id == poll_data.candidate.id:
                            item.votes += poll_data.votes
                    
        return summary, election

    def get_average_poll_by_election_id(self, election_id: str) -> Poll:
        """Returns a single Poll object that represents the average poll results of all polls for a single election. 
        Returns an error string if the election does not exist, or no poll exists for that election."""

        result = self.get_poll_summary_by_election_id(election_id)
        if isinstance(result, str):
            return result

        summary, election = result

        total_polls = len(self.get_polls_by_election_id(election_id))
        if total_polls == 0:
            return f"There are no polls for election with id {election_id}"

        average_poll = []
        for data in summary:
            average_poll.append(PollData(data.candidate,int(data.votes/total_polls)))
        
        return average_poll

    def create_poll(self, poll: PollInputModel) -> Poll:
        """Creates a new poll from the PollInputModel and the election that the poll applies to and returns the new poll"""
        # Check whether the election id is valid
        election = self.__election_repository.get_election_by_id(poll.election_id)
        if isinstance(election, str): 
            return election

        valid_region_ids = [region.id for region in self.__region_repository.get_all_regions()]
        valid_candidate_ids = [candidate.id for candidate in self.__candidate_repository.get_all_candidates()]

        for region_poll in poll.region_polls:
            # Check whether the regions are valid
            if region_poll.region not in valid_region_ids:
                return f"Region with id {region_poll.region} is not valid"
            region_poll.region = self.__region_repository.get_region_by_id(region_poll.region)
            data_list = []
            for data in region_poll.data:
                # Check whether the candidates are valid
                if data.candidate_id not in valid_candidate_ids:
                    return f"Candidate with id {data.candidate_id} is not valid"
                data_list.append(PollData(self.__candidate_repository.get_candidate_by_id(data.candidate_id), data.votes))
            region_poll.data = data_list

        return self.__poll_repository.create_poll(poll, election)
    
    def get_poll_per_region(self, election_id: str, region_id: str) -> list:
        """Returns all polls for a given region and election"""
        polls = self.__poll_repository.get_polls_by_election_id(election_id)
        polls_from_region = []
        for poll in polls:
            new_region_polls = []
            for region_poll in poll.region_polls:
                # We match the region id of the region poll to the selected regions id
                if region_poll.region.id == region_id:
                    new_region_polls.append(region_poll)
            # We set the region polls of the current poll to be only those from the selected region
            poll.region_polls = new_region_polls
            if new_region_polls != []:
                polls_from_region.append(poll)
        return polls_from_region
