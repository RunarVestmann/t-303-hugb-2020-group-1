from app.models.entities.election import Election
from app.models.input_models.election_input_model import ElectionInputModel
from app.repositories.election_repository import ElectionRepository
from app.repositories.candidate_repository import CandidateRepository
from app.repositories.region_repository import RegionRepository
from app.models.entities.poll_data import PollData
from datetime import date

class ElectionService:
    def __init__(self, election_repository: ElectionRepository, candidate_repository: CandidateRepository, region_repository: RegionRepository):
        self.__election_repository = election_repository
        self.__candidate_repository = candidate_repository
        self.__region_repository = region_repository

    def get_all_elections(self) -> [Election]:
        """Returns a list of the elections in the system"""
        return self.__election_repository.get_all_elections()

    def get_election_by_id(self, id: str) -> Election:
        """Returns an election by id, if the id doesn't exist, returns an error string"""
        return self.__election_repository.get_election_by_id(id)

    def __get_entities_from_ids(self, ids: list, get_entity_by_id) -> list:
        entities = []
        # Go through all the candidate ids and transfer them into models
        for id in ids:
            entity = get_entity_by_id(id)

            # Return the error string if an error occurs
            if isinstance(entity, str): 
                return entity

            entities.append(entity)
        return entities

    def create_election(self, election: ElectionInputModel) -> Election:
        """Creates a new election and returns the new election"""
        
        # Retrieve all the candidates
        candidates = self.__get_entities_from_ids(election.candidates, self.__candidate_repository.get_candidate_by_id)
        if isinstance(candidates, str):
            return candidates

        #Retrieve all the regions
        regions = self.__get_entities_from_ids(election.regions, self.__region_repository.get_region_by_id)
        if isinstance(regions, str):
            return regions
        
        election.candidates = candidates
        election.regions = regions

        return self.__election_repository.create_election(election)
