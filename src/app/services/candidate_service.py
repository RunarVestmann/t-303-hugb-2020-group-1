from app.models.entities.candidate import Candidate
from app.models.input_models.candidate_input_model import CandidateInputModel
from datetime import date

class CandidateService:
    def __init__(self, candidate_repository):
        self.__candidate_repository = candidate_repository
    
    def get_all_candidates(self) -> [Candidate]:
        """Returns a list of the candidates in the system"""
        return self.__candidate_repository.get_all_candidates()

    def get_candidates_by_election_id(self, election_id: str) -> [Candidate]:
        """Returns all the candidates in the election with the specified id, returns an error string if the id doesn't exist"""
        return self.__candidate_repository.get_candidates_by_election_id(election_id)

    def get_candidate_by_id(self, id: str) -> Candidate:
        """Returns a candidate by id, if the id doesn't exist, returns an error string"""
        return self.__candidate_repository.get_candidate_by_id(id)
 
    def create_candidate(self, candidate: CandidateInputModel) -> Candidate:
        """Creates a new candidate and returns the new candidate"""
        return self.__candidate_repository.create_candidate(candidate)
