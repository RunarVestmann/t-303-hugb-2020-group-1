from app.models.entities.region import Region
from app.repositories.region_repository import RegionRepository
from app.repositories.election_repository import ElectionRepository
from app.models.input_models.region_input_model import RegionInputModel

class RegionService:
    def __init__(self, region_repository: RegionRepository, election_repository: ElectionRepository):
        self.__region_repository = region_repository
        self.__election_repository = election_repository

    def get_all_regions(self) -> [Region]:
        """Returns a list of the regions in the system"""
        return self.__region_repository.get_all_regions()
    
    def get_region_by_id(self, region_id: str) -> Region:
        """Returns a poll by id, if the id doesn't exist, returns an error string"""
        return self.__region_repository.get_region_by_id(region_id)

    def get_regions_by_election_id(self, election_id: str) -> [Region]:
        """Returns a list of the Regions taking part in the specified Election"""
        election = self.__election_repository.get_election_by_id(election_id)
        if isinstance(election, str):
            return election
        return election.regions

    def create_region(self, region: RegionInputModel) -> Region:
        """Creates a new region from the RegionInputModel and returns the newly created region"""
        return self.__region_repository.create_region(region)
