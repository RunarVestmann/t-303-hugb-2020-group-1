from app.models.entities.candidate import Candidate
from app.models.input_models.candidate_input_model import CandidateInputModel
from app.views.candidate_view import CandidateView
from app.services.candidate_service import CandidateService
from app.repositories.candidate_repository import CandidateRepository

from app.models.input_models.election_input_model import ElectionInputModel
from app.views.election_view import ElectionView
from app.services.election_service import ElectionService
from app.repositories.election_repository import ElectionRepository

from app.models.input_models.poll_input_model import PollInputModel
from app.models.input_models.poll_data_input_model import PollDataInputModel
from app.views.poll_view import PollView
from app.services.poll_service import PollService
from app.repositories.poll_repository import PollRepository

from app.models.entities.region import Region
from app.models.entities.region_poll import RegionPoll
from app.models.input_models.region_input_model import RegionInputModel
from app.views.region_view import RegionView
from app.services.region_service import RegionService
from app.repositories.region_repository import RegionRepository

from app.repositories.database_context import DatabaseContext

from datetime import date

class SystemInterface:
    """The interface that allows clients to communicate with our system"""
    
    def __init__(self):
        self.__candidate_view = CandidateView(CandidateService(CandidateRepository(DatabaseContext())))
        self.__election_view = ElectionView(ElectionService(ElectionRepository(DatabaseContext()), CandidateRepository(DatabaseContext()), RegionRepository(DatabaseContext())))
        self.__poll_view = PollView(PollService(PollRepository(DatabaseContext(), ElectionRepository(DatabaseContext())), ElectionRepository(DatabaseContext()), RegionRepository(DatabaseContext()), CandidateRepository(DatabaseContext())))
        self.__region_view = RegionView(RegionService(RegionRepository(DatabaseContext()), ElectionRepository(DatabaseContext())))

    def getPollsForElection(self, electionID: str) -> str:
        """Returns all polls existing in the system for a single election.\n
           If the election does not exist, an empty list is returned"""

        print(f"getPollsForElection({electionID})")
        return self.__poll_view.get_polls_by_election_id(electionID)

    def getPollDetails(self, pollID: str) -> str:
        """Returns a single poll with the given ID.\n
           Returns an empty string if no candidate with this ID exists"""

        print(f"getPollDetails({pollID})")
        return self.__poll_view.get_poll_by_id(pollID)

    def getAveragePoll(self, electionID: str) -> str:
        """Returns a single Poll object that represents the average poll results of all polls for a single election. 
           Returns an empty string if the election does not exist, or no poll exists for that election."""
        
        print(f"getAveragePoll({electionID})")
        return self.__poll_view.get_average_poll_by_election_id(electionID)
   
    def getElectionDetails(self, electionID: str) -> str:
        """Returns a single election with the given ID.\n
           Returns an empty string if no candidate with this ID exists"""

        print(f"getElectionDetails({electionID})")
        return self.__election_view.get_election_by_id(electionID)

    def getAllElections(self) -> str:
        """Returns list of all existing elections in the system.\n
           If no election exists, an empty list is returned"""

        print("getAllElections()")
        return self.__election_view.get_all_elections()

    def getAllCandidates(self) -> str:
        """Returns list of all existing candidates in the system.\n
           If no candidate exists, an empty list is returned"""

        print("getAllCandidates()")
        return self.__candidate_view.get_all_candidates()

    def getAllRegions(self) -> str:
        """Returns list of all existing regions in the system.\n
           If no regions exists, an empty list is returned"""

        print("getAllRegions()")
        return self.__region_view.get_all_regions()

    def getPollPerRegion(self, electionID: str, regionID: str) -> str:
        """Returns all polls for a given region and election.\n
           Returns an empty list if no polls exist for the region and/or election"""
        
        print(f"getPollPerRegion({electionID}, {regionID})")
        return self.__poll_view.get_poll_per_region(electionID, regionID)
        
    def getCandidates(self, electionID: str) -> str:
        """Returns all candidates for a given election.\n
           Returns an empty list if no election with this ID exists, or the election has no candidate"""

        print(f"getCandidates({electionID})")
        return self.__candidate_view.get_candidates_by_election_id(electionID)

    def getCandidateDetails(self, candidateID: str) -> str:
        """Returns a single candidate with the given ID.\n
           Returns an empty string if no candidate with this ID exists"""

        print(f"getCandidateDetails({candidateID})")
        return self.__candidate_view.get_candidate_by_id(candidateID)

    def getRegionDetails(self, regionID: str) -> str:
        """Returns a single region with the given ID.\n
           Returns an empty string if no region with this ID exists"""

        print(f"getRegionDetails({regionID})")
        return self.__region_view.get_region_by_id(regionID)

    def createCandidate(self, name: str, birthDate: str="", politicalParty: str="", bio: str="") -> str:
        """Creates a new candidate. Upon success, the candidate is returned. Otherwise, an error message is returned"""
        
        print(f"createCandidate({name})")

        if birthDate != "":
            year, month, day = [int(val) for val in birthDate.split('-')]
            birthDate = date(year, month, day)
        
        return self.__candidate_view.create_candidate(CandidateInputModel(name, birthDate, politicalParty, bio))

    def createRegion(self, name: str, registeredVoters: int=0) -> str:
        """Creates a new region. Upon success, the region is returned. Otherwise, an error message is returned"""

        print(f"createRegion({name})")
        return self.__region_view.create_region(RegionInputModel(name, registeredVoters))

    def createElection(self, name: str, candidateIDs: [str], votingDate: str="", regionIDs: [str]=(), electionType: str="") -> str:
        """Creates a new election. Upon success, the election is returned. Otherwise, an error message is returned"""

        print(f"createElection({name})")

        candidateIDs = [str(id) for id in candidateIDs]
        regionIDs = [str(id) for id in regionIDs]

        if votingDate != "":
            year, month, day = [int(val) for val in votingDate.split('-')]
            votingDate = date(year, month, day)

        return self.__election_view.create_election(ElectionInputModel(name, candidateIDs, votingDate, regionIDs, electionType))

    def createPoll(self, dataArray: [RegionPoll], electionID: str, organization: str="") -> str:
        """Creates a new poll. Upon success, the poll is returned. Otherwise, an error message is returned"""

        print(f"createPoll()")

        poll = PollInputModel(organization, dataArray, electionID)

        region_polls_list = []
        for region_poll in poll.region_polls:
            poll_data_list = []
            for poll_data in region_poll['data']:
                poll_data = PollDataInputModel(poll_data['candidate'], int(poll_data['votes']))
                poll_data_list.append(poll_data)
            region_poll = RegionPoll(poll_data_list, region_poll['region'])
            region_polls_list.append(region_poll)
        poll = PollInputModel(poll.conductor, region_polls_list, poll.election_id)  

        return self.__poll_view.create_poll(poll)
