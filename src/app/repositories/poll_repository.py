from app.models.entities.candidate import Candidate
from app.models.entities.poll import Poll
from app.models.input_models.poll_input_model import PollInputModel
from app.models.entities.poll_data import PollData
from app.models.entities.region_poll import RegionPoll
from app.models.entities.region import Region
from app.models.entities.election import Election
from app.repositories.database_context import DatabaseContext
from app.repositories.election_repository import ElectionRepository
from datetime import date
import copy

class PollRepository:
    def __init__(self, database_context, election_repository: ElectionRepository):
        self.__database_context = database_context
        self.__election_repository = election_repository

    def get_all_polls(self) -> [Poll]:
        """Returns a list of the polls in the system"""
        return copy.deepcopy(self.__database_context.polls)

    def get_poll_by_id(self, id: str) -> Poll:
        """Returns a poll by id, if the id doesn't exist, returns an error string"""
        for poll in self.__database_context.polls:
            if poll.id == id:
                return copy.deepcopy(poll)
        return f"Poll with id {id} was not found"

    def get_polls_by_election_id(self, election_id: str) -> [Poll]:
        """Returns a list of all the polls in an election with the specified id, if the id doesn't exist, returns an error string"""
        
        # Check if election exists
        election = self.__election_repository.get_election_by_id(election_id)
        if isinstance(election, str): return election
        
        polls = []
        for poll in self.__database_context.polls:
            if poll.election.id == election_id:
                polls.append(copy.deepcopy(poll))
        return polls

    def create_poll(self, poll: PollInputModel, election: Election) -> Poll:
        """Creates a new poll from the PollInputModel and returns the newly created poll"""
        new_poll = Poll(str(self.__database_context.next_poll_id), poll.conductor, poll.region_polls, election)
        self.__database_context.next_poll_id += 1
        self.__database_context.polls.append(new_poll)
        return new_poll
