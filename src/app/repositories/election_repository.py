from app.models.input_models.election_input_model import ElectionInputModel
from app.models.entities.election import Election
from app.repositories.database_context import DatabaseContext
from app.models.entities.poll_data import PollData
import copy

class ElectionRepository:
    def __init__(self, database_context):
        self.__database_context = database_context

    def get_all_elections(self) -> [Election]:
        """Returns a list of the elections in the system"""
        return copy.deepcopy(self.__database_context.elections)
    
    def get_election_by_id(self, id: str) -> Election:
        """Returns an elections by id, if the id doesn't exist, returns an error string"""
        for election in self.__database_context.elections:
            if election.id == id:
                return copy.deepcopy(election)
        return f"Election with id {id} was not found"

    def create_election(self, election: ElectionInputModel) -> Election:
        """Creates a new election and returns the newly created election"""
        new_election = Election(str(self.__database_context.next_election_id), election.name, election.candidates,\
                                election.voting_date, election.regions, election.election_type)
        self.__database_context.next_election_id += 1
        self.__database_context.elections.append(new_election)
        return new_election
