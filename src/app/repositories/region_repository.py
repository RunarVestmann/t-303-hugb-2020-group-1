from app.models.entities.region import Region
from app.models.entities.election import Election
from app.repositories.database_context import DatabaseContext
from app.models.input_models.region_input_model import RegionInputModel
import copy

class RegionRepository:
    def __init__(self, database_context: DatabaseContext):
        self.__database_context = database_context

    def get_all_regions(self) -> [Region]:
        """Returns a list of the regions in the system"""
        return copy.deepcopy(self.__database_context.regions)
    
    def get_region_by_id(self, region_id: str) -> Region:
        """Returns a poll by id, if the id doesn't exist, returns an error string"""
        for region in self.__database_context.regions:
            if region.id == region_id:
                return copy.deepcopy(region)
        return f"Region with id {region_id} was not found"

    def create_region(self, region: RegionInputModel) -> Region:
        """Creates a new region from the RegionInputModel and returns the newly created region"""
        new_region = Region(str(self.__database_context.next_region_id), region.name, region.registered_voters)
        self.__database_context.next_region_id += 1
        self.__database_context.regions.append(new_region)
        return new_region
