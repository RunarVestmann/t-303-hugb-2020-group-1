from app.models.entities.candidate import Candidate
from app.models.entities.election import Election
from app.models.entities.poll import Poll
from app.models.entities.poll_data import PollData
from app.models.entities.region_poll import RegionPoll
from app.models.entities.region import Region
from datetime import date

class DatabaseContext:
    """
    Creates dummy data for testing
    """
    candidates = [
        Candidate(
            "1",
            "Donald J. Trump",
            date(1946, 6, 14),
            "Republican Party",
            "Currently the President of the United States"
        ),
        Candidate(
            "2",
            "Joe Biden",
            date(1942, 11, 20),
            "Democratic Party",
            "Former Vice President of the United States"
        )
    ]

    regions = [
        Region("1", "Alabama", 200),
        Region("2", "Alaska", 200),
        Region("3", "Arizona", 200),
        Region("4", "Arkansas", 200),
        Region("5", "California", 200),
        Region("6", "Colorado", 200),
        Region("7", "Connecticut", 200),
        Region("8", "Delaware", 200),
        Region("9", "D.C.", 200),
        Region("10", "Florida", 200),
        Region("11", "Georgia", 200),
        Region("12", "Hawaii", 200),
        Region("13", "Idaho", 200),
        Region("14", "Illinois", 200),
        Region("15", "Iowa", 200),
        Region("16", "Kansas", 200),
        Region("17", "Kentucky", 200),
        Region("18", "Lousiana", 200),
        Region("19", "Maine", 200),
        Region("20", "Maryland", 200),
        Region("21", "Massachusetts", 200),
        Region("22", "Michigan", 200),
        Region("23", "Minnesota", 200),
        Region("24", "Mississippi", 200),
        Region("25", "Missouri", 200),
        Region("26", "Montana", 200),
        Region("27", "Nebraska", 200),
        Region("28", "Nevada", 200),
        Region("29", "New Hamphire", 200),
        Region("30", "New Jersey", 200),
        Region("31", "New Mexico", 200),
        Region("32", "New York", 200),
        Region("33", "North Carolina", 200),
        Region("34", "North Dakota", 200),
        Region("35", "Ohio", 200),
        Region("36", "Oklahoma", 200),
        Region("37", "Oregon", 200),
        Region("38", "Pennsylvania", 200),
        Region("39", "Rhode Island", 200),
        Region("40", "South Carolina", 200),
        Region("41", "South Dakota", 200),
        Region("42", "Tennessee", 200),
        Region("43", "Texas", 200),
        Region("44", "Utah", 200),
        Region("45", "Vermont", 200),
        Region("46", "Virginia", 200),
        Region("47", "Washington", 200),
        Region("48", "West Virgina", 200),
        Region("49", "Wisconsin", 200),
        Region("50", "Wyoming", 200),
    ]

    elections = [
        Election(
            "1", 
            "United States Presidential election, 2020", 
            candidates[:2], 
            date(2020, 11, 3),
            regions[:51],
            "Presidential"
        )
    ]

    polls = [
        Poll("1", "CNN",
        [
            RegionPoll([
            PollData(candidates[0], 33),
            PollData(candidates[1], 133),
            ], regions[2]),

            RegionPoll([
            PollData(candidates[0], 22),
            PollData(candidates[1], 14),
            ], regions[1]),

            RegionPoll([
            PollData(candidates[0], 78),
            PollData(candidates[1], 44),
            ], regions[0])
        ],
            elections[0]
        ),
        Poll("2", "New York Times",
        [
            RegionPoll([
            PollData(candidates[0], 55),
            PollData(candidates[1], 18),
            ], regions[33]),

            RegionPoll([
            PollData(candidates[0], 45),
            PollData(candidates[1], 15),
            ], regions[22]),

            RegionPoll([
            PollData(candidates[0], 33),
            PollData(candidates[1], 77),
            ], regions[5])
        ],
            elections[0]
        )
    ]

    next_candidate_id = len(candidates)+1
    next_election_id = len(elections)+1
    next_region_id = len(regions)+1
    next_poll_id = len(polls)+1
