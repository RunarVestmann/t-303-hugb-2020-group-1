from app.repositories.database_context import DatabaseContext
from datetime import date
from app.models.entities.candidate import Candidate
from app.models.input_models.candidate_input_model import CandidateInputModel
import copy

class CandidateRepository:
    def __init__(self, database_context):
        self.__database_context = database_context

    def get_all_candidates(self) -> [Candidate]:
        """Returns a list of the candidates in the system"""
        return copy.deepcopy(self.__database_context.candidates)

    def get_candidate_by_id(self, id: str) -> Candidate:
        """Returns a candidate by id, if the id doesn't exist, returns an error string"""
        for candidate in self.__database_context.candidates:
            if candidate.id == id:
                return copy.deepcopy(candidate)
        return f"Candidate with id {id} was not found"

    def get_candidates_by_election_id(self, election_id: str) -> [Candidate]:
        """Returns all the candidates in the election with the specified id, if the id doesn't exist, returns an error string"""
        for election in self.__database_context.elections:
            if election.id == election_id:
                return copy.deepcopy(election.candidates)
        return f"Election with id {election_id} was not found"

    def create_candidate(self, candidate: CandidateInputModel) -> Candidate:
        """Creates a new candidate and returns the newly created candidate"""
        new_candidate = Candidate(
            str(self.__database_context.next_candidate_id), 
            candidate.name,
            candidate.birthdate,
            candidate.political_party,
            candidate.bio 
        )
        self.__database_context.next_candidate_id += 1
        self.__database_context.candidates.append(new_candidate)
        return new_candidate
    