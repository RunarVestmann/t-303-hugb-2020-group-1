class Region:
    """
    A class that represents a region.
    
    Attributes
    ----------    
    id: str
        unique identifier for the region
    name: str
        the name of the region
    registered_voters: int
        the number of registered voters
    """
    def __init__(self, id: str, name: str, registered_voters: int):
        self.id = id
        self.name = name
        self.registered_voters = registered_voters

    def to_dict(self):
        return {
            "regionID": self.id,
            "name": self.name,
            "registeredVoters": self.registered_voters
        }
