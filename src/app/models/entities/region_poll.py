from app.models.entities.region import Region

class RegionPoll:
    """
    A class that represents a list of PollData.
    
    Attributes
    ----------    
    data: list
        a list of PollData
    region: Region
        Region that shows where the list of PollData is from
    """
    def __init__(self, data: list, region: Region):
        self.data = data
        self.region = region

    def to_dict(self):
        return {
            "data": [data.to_dict() for data in self.data],
            "region": "" if not self.region else self.region.to_dict()
        }
