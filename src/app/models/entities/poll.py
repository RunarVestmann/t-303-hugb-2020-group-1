from app.models.entities.election import Election

class Poll:
    """
    A class that represents a poll.
    
    Attributes
    ----------    
    id: str
        unique identifier for the poll
    conductor: str
        the name of the conductor of the poll 
    region_polls: list
        a RegionPoll
    election: Election
        the election the poll is collecting data for
    """
    def __init__(self, id: str, conductor: str, region_polls: list, election: Election):
        self.id = id
        self.conductor = conductor
        self.region_polls = region_polls
        self.election = election

    def to_dict(self):
        return {
            "pollID": self.id,
            "dataArray": [region_poll.to_dict() for region_poll in self.region_polls]
        }
