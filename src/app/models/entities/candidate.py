from datetime import date

class Candidate:
    """
    A class that represents a candidate.
    
    Attributes
    ----------    
    id: str
        unique identifier for the candidate
    name: str
        the name of the candidate
    birthdate: date
        the birthdate of the candidate
    political_party: str
        the name of the political party the candidate is affiliated with 
    bio: str
        A short bio of the candidate
    """
    def __init__(self, id: str,  name: str, birthdate: date, political_party: str, bio: str):
        self.id = id
        self.name = name
        self.birthdate = birthdate
        self.political_party = political_party
        self.bio = bio

    def to_dict(self):
        return {
            "candidateID": self.id,
            "name": self.name,
            "birthDate": "" if not self.birthdate else self.birthdate.strftime("%d-%m-%Y"),
            "politicalParty": self.political_party,
            "bio": self.bio
        }
