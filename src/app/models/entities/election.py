from datetime import date

class Election:
    """
    A class that represents an election.
    
    Attributes
    ----------    
    id: str
        unique identifier for the election
    name: str
        the name of the election
    candidates: list
        a list containing the candidates of the election
    voting_date: date
        the date of when the election takes place
    regions: list
        a list containing the regions that are partaking in the election
    election_type: str
        a string that lets you know what type of election this is
    """
    def __init__(self, id: str, name: str, candidates: list, voting_date: date, regions: list, election_type: str):
        self.id = id
        self.name = name
        self.candidates = candidates
        self.voting_date = voting_date
        self.regions = regions
        self.type = election_type

    def to_dict(self):
        return {
            "electionID": self.id,
            "name": self.name,
            "candidates": [candidate.to_dict() for candidate in self.candidates],
            "votingDate": "" if not self.voting_date else self.voting_date.strftime("%d/%m/%Y"),
            "regions": [region.to_dict() for region in self.regions]
        }
