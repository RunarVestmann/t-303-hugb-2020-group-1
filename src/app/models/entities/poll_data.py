from app.models.entities.candidate import Candidate

class PollData:
    """
    A class that represents PollData.
    
    Attributes
    ----------    
    candidate: Candidate
        the candidate the poll data is for
    votes: int
        how many votes the candidate got.
    """
    def __init__(self, candidate: Candidate, votes: int):
        self.candidate = candidate
        self.votes = votes

    def to_dict(self):
        return {
            "candidate": self.candidate.to_dict(),
            "votes": self.votes
        }
