class PollDataInputModel:
    """
    A class that represents the input you need to create new PollData.
    
    Attributes
    ----------    
    candidate: Candidate
        the candidate the poll data is for
    votes: int
        how many votes the candidate got.
    """
    def __init__(self, candidate_id: str, votes: int):
        self.candidate_id = candidate_id
        self.votes = votes
