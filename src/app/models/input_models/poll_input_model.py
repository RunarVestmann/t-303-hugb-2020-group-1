class PollInputModel:
    """
    A class that creates a new poll.
    
    Attributes
    ----------    
    conductor: str
        the name of the conductor of the poll 
    region_polls: list
        a RegionPoll
    election: Election
        the election the poll is collecting data for
    """
    def __init__(self, conductor: str, region_polls: list, election_id: str):
        self.conductor = conductor
        self.region_polls = region_polls
        self.election_id = election_id
