from datetime import date

class CandidateInputModel:
    """
    A class that represents the input you need to create a new Candidate.
    
    Attributes
    ----------    
    name: str
        the name of the candidate
    birthdate: date
        the birthdate of the candidate
    political_party: str
        the name of the political party the candidate is affiliated with 
    bio: str
        A short bio of the candidate
    """
    def __init__(self, name: str, birthdate: date, political_party: str, bio: str):
        self.name = name
        self.birthdate = birthdate
        self.political_party = political_party
        self.bio = bio
