class RegionInputModel:
    """
    A class that represents the input you need to create a new Region
    
    Attributes
    ----------    
    name: str
        the name of the region
    registered_voters: int
        the number of registered voters
    """
    def __init__(self, name: str, registered_voters: int):
        self.name = name
        self.registered_voters = registered_voters
