from datetime import date

class ElectionInputModel:
    """
    A class that represents the input you need to create a new Election.
    
    Attributes
    ----------    
    name: str
        the name of the election
    candidates: list
        a list containing the candidates of the election
    voting_date: date
        the date of when the election takes place
    regions: list
        a list containing the regions that are partaking in the election
    election_type: str
        a string that lets you know what type of election this is
    """
    def __init__(self, name: str, candidates: list, voting_date: date, regions: list, election_type: str):
        self.name = name
        self.candidates = candidates
        self.voting_date = voting_date
        self.regions = regions
        self.election_type = election_type