import json
from app.models.entities.election import Election
from app.services.election_service import ElectionService
from app.models.input_models.election_input_model import ElectionInputModel

class ElectionView:
    def __init__(self, election_service: ElectionService):
        self.__election_service = election_service

    def get_all_elections(self) -> str:
        """Returns a list of the elections in the system"""
        elections = self.__election_service.get_all_elections()
        return json.dumps({"msg": [election.to_dict() for election in elections]})

    def get_election_by_id(self, id: str) -> str:
        """Returns an election by id, if the id doesn't exist, returns an empty string"""
        election = self.__election_service.get_election_by_id(id)
        return json.dumps({"msg": "" if isinstance(election, str) else election.to_dict()})

    def create_election(self, election: ElectionInputModel) -> str:
        """Creates a new election and returns the new election"""
        new_election = self.__election_service.create_election(election)
        return json.dumps({"msg": new_election if isinstance(new_election, str) else new_election.to_dict()})
