from app.models.entities.region import Region
from app.services.region_service import RegionService
from app.models.input_models.region_input_model import RegionInputModel
import json

class RegionView:
    def __init__(self, region_service: RegionService):
        self.__region_service = region_service

    def get_all_regions(self) -> str:
        """Returns a list of the regions in the system"""
        return json.dumps({"msg": [region.to_dict() for region in self.__region_service.get_all_regions()]})
    
    def get_region_by_id(self, region_id: str) -> str:
        """Returns a single region with the given ID. Returns an empty string if no region with this ID exists"""
        region = self.__region_service.get_region_by_id(region_id)
        return json.dumps({"msg": "" if isinstance(region, str) else region.to_dict()})
    
    def get_regions_by_election_id(self, election_id: str) -> str:
        """Returns a list of the Regions taking part in the specified Election"""
        regions = self.__region_service.get_regions_by_election_id(election_id)
        return json.dumps({"msg": [] if isinstance(regions, str) else [region.to_dict() for region in regions]})

    def create_region(self, region: RegionInputModel) -> str:
        """Creates a new region from the RegionInputModel and returns the newly created region"""
        return json.dumps({"msg": self.__region_service.create_region(region).to_dict()})
