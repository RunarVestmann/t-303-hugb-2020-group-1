from app.models.entities.poll import Poll
from app.models.input_models.poll_input_model import PollInputModel
from app.services.poll_service import PollService
import json

class PollView:
    def __init__(self, poll_service: PollService):
        self.__poll_service = poll_service
    
    def get_all_polls(self) -> str:
        """Returns a list of the polls in the system"""
        polls = self.__poll_service.get_all_polls()
        return json.dumps({"msg": [poll.to_dict() for poll in polls]})
    
    def get_poll_by_id(self, id: str) -> str:
        """Returns a poll by id, returns an empty string if the id doesn't exist"""
        poll = self.__poll_service.get_poll_by_id(id)
        return json.dumps({"msg": "" if isinstance(poll, str) else poll.to_dict()})

    def get_polls_by_election_id(self, election_id: str) -> str:
        """Returns all the polls in the election with the specified id, returns an empty list if the id doesn't exist"""
        polls = self.__poll_service.get_polls_by_election_id(election_id)
        return json.dumps({"msg": [] if isinstance(polls, str) else [poll.to_dict() for poll in polls]})

    def get_poll_summary_by_election_id(self, election_id: str) -> list:
        """Returns a summary of all the polls for a specified election, i.e. returns how many votes each candidate in an election has in all the polls in the system"""
        summary = self.__poll_service.get_poll_summary_by_election_id(election_id)
        if isinstance(summary, str):
            return json.dumps({"msg": summary})
        poll_data = [summary.to_dict() for summary in summary[0]]
        election_data = summary[1].to_dict()
        summary_list = json.dumps([poll_data, election_data])
        return summary_list

    def create_poll(self, poll: PollInputModel) -> str:
        """Creates a new poll from the PollInputModel and the election that the poll applies to and returns the new poll"""
        new_poll = self.__poll_service.create_poll(poll)
        return json.dumps({"msg": new_poll if isinstance(new_poll, str) else new_poll.to_dict()})

    def get_poll_per_region(self, election_id: str, region_id: str) -> str:
        """Returns all polls for a given region and election"""
        polls = self.__poll_service.get_poll_per_region(election_id, region_id)
        return json.dumps({"msg": [] if isinstance(polls, str) else [poll.to_dict() for poll in polls]})

    def get_average_poll_by_election_id(self, election_id: str) -> str:
        """Returns a single Poll object that represents the average poll results of all polls for a single election"""
        data = self.__poll_service.get_average_poll_by_election_id(election_id)
        return json.dumps({"msg": [] if isinstance(data, str) else [poll_data.to_dict() for poll_data in data]})
