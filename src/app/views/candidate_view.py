from app.models.entities.candidate import Candidate
from app.models.input_models.candidate_input_model import CandidateInputModel
import json

class CandidateView:
    def __init__(self, candidate_service):
        self.__candidate_service = candidate_service
    
    def get_all_candidates(self) -> str:
        """Returns a list of the candidates in the system"""
        return json.dumps({"msg": [candidate.to_dict() for candidate in self.__candidate_service.get_all_candidates()]})

    def get_candidates_by_election_id(self, election_id: str) -> str:
        """Returns all the candidates in the election with the specified id, if the id doesn't exist, returns an empty list"""
        candidates = self.__candidate_service.get_candidates_by_election_id(election_id)
        return json.dumps({"msg": [] if isinstance(candidates, str) else [candidate.to_dict() for candidate in candidates]})

    def get_candidate_by_id(self, id: str) -> str:
        """Returns a candidate by id, if the id doesn't exist, returns an empty string"""
        candidate = self.__candidate_service.get_candidate_by_id(id)
        return json.dumps({"msg": "" if isinstance(candidate, str) else candidate.to_dict()})

    def create_candidate(self, candidate: CandidateInputModel) -> str:
        """Creates a new candidate and returns the new candidate"""
        return json.dumps({"msg": self.__candidate_service.create_candidate(candidate).to_dict()})
