import asyncio
import websockets
import json

class Client:
    """A simple class that can be used to test the websocket server"""
    def __init__(self, url: str = "ws://localhost", port: int = 8080):
        self.url = url
        self.port = port

    async def send_json(self, message: str):
        """Sends a message and returns a response"""
        async with websockets.connect(f"ws://localhost:{self.port}") as websocket:
            await websocket.send(message)
            response = await websocket.recv()
            return response
    
    def print_menu(self):
        print("(C)all method\t(W)rite it all by hand")

    async def run(self):
        """Keeps prompting for input to send to the server until no input is given"""
        self.print_menu()

        while (user_input := input("Enter option: ").lower()) != "":
            if user_input == "c":
                
                message = self.prompt_user_to_call_method()
                if not message: 
                    continue  
            elif user_input == "w":
                message = input("Enter JSON: ")
            else:
                print("Please enter one of the letters in the brackets:")
                self.print_menu()
                continue
            
            response = await client.send_json(message)
            print(f"Response: {response}")

            self.print_menu()

    def prompt_user_to_call_method(self):
        method = input("Enter method to call: ")
        if not method:
            return method

        argument_dict = {}
        while (argument := input("Enter argument name: ")) != "":
            argument_dict[argument] = input(f"Enter value for {argument}: ")

        return json.dumps({"op": method, "data" : argument_dict})

if __name__ == "__main__":
    client = Client()
    asyncio.get_event_loop().run_until_complete(client.run())
    