import unittest
from test.mocks.region_mock import RegionMock
from app.views.region_view import RegionView
from app.models.input_models.region_input_model import RegionInputModel
import json

class TestRegionView(unittest.TestCase):
    def setUp(self):
        self.__region_view = RegionView(RegionMock())

    def test_get_all_regions(self):
        regions = json.loads(self.__region_view.get_all_regions())["msg"]
        self.assertEqual(len(regions), 50)


    def test_get_region_by_id(self):
        region = json.loads(self.__region_view.get_region_by_id("1"))["msg"]
        self.assertEqual(region["regionID"],"1")

        # Expect there to be an empty string when region_id doesn't exist
        region = json.loads(self.__region_view.get_region_by_id("400"))["msg"]
        self.assertEqual(isinstance(region, str), True)

    def test_get_regions_by_election_id(self):
        regions = json.loads(self.__region_view.get_regions_by_election_id("1"))["msg"]
        self.assertEqual(len(regions), 50)

        # Expect there to be an empty list when election_id doesn't exist
        regions = json.loads(self.__region_view.get_regions_by_election_id("400"))["msg"]
        self.assertEqual(len(regions), 0)

    def test_create_region(self):
        region = RegionInputModel("Georgia", 250)
        region_count = len(json.loads(self.__region_view.get_all_regions())["msg"])
        self.__region_view.create_region(region)
        self.assertEqual(len(json.loads(self.__region_view.get_all_regions())["msg"]), region_count+1)
