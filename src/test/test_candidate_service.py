import unittest
from test.mocks.candidate_mock import CandidateMock
from app.services.candidate_service import CandidateService
from app.models.input_models.candidate_input_model import CandidateInputModel
from datetime import date

class TestCandidateService(unittest.TestCase):
    def setUp(self):
        self.__candidate_service = CandidateService(CandidateMock())

    def test_get_all_candidates(self):
        self.assertEqual(len(self.__candidate_service.get_all_candidates()), 2)

    def test_get_candidate_by_id(self):
        candidate = self.__candidate_service.get_candidate_by_id("1")
        #We expect that we get candidate with id 1
        self.assertEqual(candidate.id, "1")

        candidate2 = self.__candidate_service.get_candidate_by_id("100")
        #Check to see if an error string is returned when an invalid candidate id is entered
        self.assertEqual(isinstance(candidate2, str), True)


    def test_get_candidates_by_election_id(self):
        candidates = self.__candidate_service.get_candidates_by_election_id("1")
        self.assertEqual(len(candidates), 2)

        #Check to see if an error string is returned when an invalid election id is entered
        candidates = self.__candidate_service.get_candidates_by_election_id("100")
        self.assertEqual(isinstance(candidates, str), True)

    def test_create_candidate(self):
        candidate_count = len(self.__candidate_service.get_all_candidates())
        self.__candidate_service.create_candidate(CandidateInputModel("John Doe", date(1980, 6, 6), "Republican Party", "Just some guy"))
        self.assertEqual(len(self.__candidate_service.get_all_candidates()), candidate_count+1)

if __name__ == "__main__":
    unittest.main()