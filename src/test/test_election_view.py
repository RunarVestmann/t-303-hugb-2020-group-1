import unittest
import json
from datetime import date
from test.mocks.election_mock import ElectionMock
from app.views.election_view import ElectionView
from app.models.input_models.election_input_model import ElectionInputModel

class TestElectionView(unittest.TestCase):
    def setUp(self):
        self.__election_view = ElectionView(ElectionMock())

    def test_get_all_elections(self):
        elections = json.loads(self.__election_view.get_all_elections())["msg"]
        # We expect to get two elections back from the mock data
        self.assertEqual(len(json.loads(self.__election_view.get_all_elections())["msg"]), 2)
        for election in elections:
        # Election should have the following keys
            self.assertEqual('electionID' in election, True)
            self.assertEqual('id' in election, False)
            self.assertEqual('name' in election, True)
            self.assertEqual('candidates' in election, True)
            self.assertEqual('votingDate' in election, True)
            self.assertEqual('regions' in election, True)

    
    def test_get_election_by_id(self):
        election = json.loads(self.__election_view.get_election_by_id("1"))["msg"]
        # Election should have the same id that was used to find it
        self.assertEqual(election['electionID'], "1")
        # Check if the election has the correct key names
        self.assertEqual('electionID' in election, True)
        self.assertEqual('id' in election, False)
        self.assertEqual('name' in election, True)
        self.assertEqual('candidates' in election, True)
        self.assertEqual('votingDate' in election, True)
        self.assertEqual('regions' in election, True)
        # We expect to get an error string from the view layer since there is no election with id 99
        not_found_election = json.loads(self.__election_view.get_election_by_id("99"))["msg"]
        self.assertEqual(isinstance(not_found_election, str), True)

    def test_create_election(self):
        election = ElectionInputModel("United States Presedential election, 2028", ["1", "2"], date(2028, 11, 3), ["1", "2"], "Presidential")
        # We get the number of elections in the system
        elections_count = len(json.loads(self.__election_view.get_all_elections())["msg"])
        new_election = json.loads(self.__election_view.create_election(election))["msg"]
        # We expect the number of elections in the system to have increased by one
        self.assertEqual(len(json.loads(self.__election_view.get_all_elections())["msg"]), elections_count+1)
        # We expect the response to be the newly created election with the following keys
        self.assertEqual('electionID' in new_election, True)
        self.assertEqual('id' in new_election, False)
        self.assertEqual('name' in new_election, True)
        self.assertEqual('candidates' in new_election, True)
        self.assertEqual('votingDate' in new_election, True)
        self.assertEqual('regions' in new_election, True)

        # Failure test - Candidate with id 20 does not exist
        election2 = ElectionInputModel("United States Presedential election, 2028", ["1", "20"], date(2028, 11, 3), ["1", "2"], "Presidential")
        elections_count = len(json.loads(self.__election_view.get_all_elections())["msg"])
        fail_new_election = json.loads(self.__election_view.create_election(election2))["msg"]
        # We expect that election2 does not get created because one of the candidates is not in the system
        self.assertEqual(len(json.loads(self.__election_view.get_all_elections())["msg"]), elections_count)
        # If the election creation fails, then the response is a string containing an error message of sorts
        self.assertEqual(isinstance(fail_new_election, str), True)

if __name__ == "__main__":
    unittest.main()