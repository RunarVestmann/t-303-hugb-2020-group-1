from test.mocks.election_mock import ElectionMock
from test.mocks.candidate_mock import CandidateMock
from test.mocks.region_mock import RegionMock
from app.services.election_service import ElectionService
from app.models.input_models.election_input_model import ElectionInputModel
from datetime import date
import unittest

class TestElectionService(unittest.TestCase):
    def setUp(self):
        self.__election_service = ElectionService(ElectionMock(), CandidateMock(), RegionMock())
    
    def test_get_all_elections(self):
        #We expect to get two elections back from the mock data
        self.assertEqual(len(self.__election_service.get_all_elections()), 2)

    def test_get_election_by_id(self):
        self.assertEqual(self.__election_service.get_election_by_id("1").id, "1")

        #We expect to get an error string since there is no election with id 99
        self.assertEqual(isinstance(self.__election_service.get_election_by_id("99"), str), True)

    def test_create_election(self):
        election = ElectionInputModel("United States Presedential election, 2028", ["1", "2"], date(2028, 11, 3), [], "Presidential")
        #We get the number of elections in the system
        elections_count = len(self.__election_service.get_all_elections())
        self.__election_service.create_election(election)
        #We expect the number of elections in the system to have increased by one
        self.assertEqual(len(self.__election_service.get_all_elections()), elections_count+1)

        election2 = ElectionInputModel("United States Presedential election, 2028", ["1", "20"], date(2028, 11, 3), [], "Presidential")
        elections_count = len(self.__election_service.get_all_elections())
        self.__election_service.create_election(election2)
        #We expect that election2 does not get created because one of the candidates is not in the system
        self.assertEqual(len(self.__election_service.get_all_elections()), elections_count)

if __name__ == "__main__":
    unittest.main()