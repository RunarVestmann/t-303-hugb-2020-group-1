# import sys
# sys.path.append(sys.path[0] + "/..")
from test.mocks.region_mock import RegionMock
from test.mocks.poll_mock import PollMock
from test.mocks.election_mock import ElectionMock
from test.mocks.candidate_mock import CandidateMock
from app.models.entities.poll import Poll
from app.models.input_models.poll_input_model import PollInputModel
from app.models.input_models.poll_data_input_model import PollDataInputModel
from app.models.entities.poll_data import PollData
from app.models.entities.region_poll import RegionPoll
from app.services.poll_service import PollService
from app.models.entities.candidate import Candidate
from app.models.entities.region import Region
from datetime import date
import unittest

class TestPollService(unittest.TestCase):
    def setUp(self):
        self.__poll_service = PollService(PollMock(), ElectionMock(), RegionMock(), CandidateMock())

    def test_get_all_polls(self):
        # Since there's only one poll in the mock we expect there to be returned a list with one element
        self.assertEqual(len(self.__poll_service.get_all_polls()), 2)

    def test_get_poll_by_id(self):
        poll = self.__poll_service.get_poll_by_id("1")
        self.assertEqual(poll.id, "1")

        # Since there's no poll with id 123 in the mock we expect None
        poll = self.__poll_service.get_poll_by_id("123")
        self.assertEqual(isinstance(poll, str), True)

    def test_get_polls_by_election_id(self):
        # Expect there to be two polls related to election with id 1
        polls = self.__poll_service.get_polls_by_election_id("1")
        self.assertEqual(len(polls), 2)

        # Expect there to be an error string when an election id doesn't exist
        polls = self.__poll_service.get_polls_by_election_id("400")
        self.assertEqual(isinstance(polls, str), True)

    def test_get_poll_summary_by_election_id(self):
        poll_summary = self.__poll_service.get_poll_summary_by_election_id("1")
        poll_data_candidate_1 = poll_summary[0][0].votes
        poll_data_candidate_2 = poll_summary[0][1].votes
        election_id = poll_summary[1].id
        #We expect to get 133 votes for candidate 1 and 191 for candidate 2 from the summary of the polls
        #For election 1
        self.assertEqual(((poll_data_candidate_1,poll_data_candidate_2),election_id), ((304,375),"1"))

        poll_summary = self.__poll_service.get_poll_summary_by_election_id("5")
        #We expect to get an error string from the poll summary for an election that does not exist in the database
        self.assertEqual(isinstance(poll_summary, str), True)

    def test_create_poll(self):
        poll = PollInputModel(
            "Gallup",
            [
                RegionPoll([
                    PollDataInputModel("1", 40),
                    PollDataInputModel("2", 133),
                ], "1"),

                RegionPoll([
                    PollDataInputModel("1", 22),
                    PollDataInputModel("2", 14),
                ], "2"),

                RegionPoll([
                    PollDataInputModel("1", 78),
                    PollDataInputModel("2", 44),
                ], "3")
            ], 
            "1"
        )

        # Get how many polls there were before we created a new one
        poll_count = len(self.__poll_service.get_all_polls())
    
        self.__poll_service.create_poll(poll)
        
        # Expected result is that the poll count has increased by one
        self.assertEqual(len(self.__poll_service.get_all_polls()), poll_count+1)

if __name__ == "__main__":
    unittest.main()