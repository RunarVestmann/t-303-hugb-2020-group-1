import unittest
import json
from test.mocks.candidate_mock import CandidateMock
from app.views.candidate_view import CandidateView
from app.models.input_models.candidate_input_model import CandidateInputModel
from datetime import date

class TestCandidateView(unittest.TestCase):
    def setUp(self):
        self.__candidate_view = CandidateView(CandidateMock())

    def test_get_all_candidates(self):
        candidates = json.loads(self.__candidate_view.get_all_candidates())["msg"]
        self.assertEqual(len(candidates), 2)

    def test_get_candidate_by_id(self):
        candidate = json.loads(self.__candidate_view.get_candidate_by_id("1"))["msg"]
        #We expect that we get the candidate with id 1
        self.assertEqual(candidate.get("candidateID"), "1")
        
        #Check to see if an error string is returned when an invalid candidate id is entered
        candidate2 = json.loads(self.__candidate_view.get_candidate_by_id("100"))["msg"]
        self.assertEqual(isinstance(candidate2, str), True)


    def test_get_candidates_by_election_id(self):
        candidates = json.loads(self.__candidate_view.get_candidates_by_election_id("1"))["msg"]
        self.assertEqual(len(candidates), 2)

        #Check to see if an error string is returned when an invalid election id is entered
        candidates = json.loads(self.__candidate_view.get_candidates_by_election_id("100"))["msg"]
        self.assertEqual(candidates, [])

    def test_create_candidate(self):
        candidate_count = len(json.loads(self.__candidate_view.get_all_candidates())["msg"])
        self.__candidate_view.create_candidate(CandidateInputModel("John Doe", date(1980, 6, 6), "Republican Party", "Just some guy"))
        self.assertEqual(len(json.loads(self.__candidate_view.get_all_candidates())["msg"]), candidate_count+1)

if __name__ == "__main__":
    unittest.main()