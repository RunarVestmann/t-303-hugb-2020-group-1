import unittest
from datetime import date
from app.models.entities.election import Election
from app.models.entities.candidate import Candidate
from app.models.entities.region import Region
from app.models.input_models.region_input_model import RegionInputModel

class RegionMock:
    def __init__(self):
        self.__next_region_id = 1

        self.__candidates = [
            Candidate(
                "1",
                "Donald J. Trump",
                date(1946, 6, 14),
                "Republican Party",
                "Currently the President of the United States"
            ),
            Candidate(
                "2",
                "Joe Biden",
                date(1942, 11, 20),
                "Democratic Party",
                "Former Vice President of the United States"
            )
        ]

        self.__regions = [
            Region("1", "Alabama", 200),
            Region("2", "Alaska", 200),
            Region("3", "Arizona", 200),
            Region("4", "Arkansas", 200),
            Region("5", "California", 200),
            Region("6", "Colorado", 200),
            Region("7", "Connecticut", 200),
            Region("8", "Delaware", 200),
            Region("9", "D.C.", 200),
            Region("10", "Florida", 200),
            Region("11", "Georgia", 200),
            Region("12", "Hawaii", 200),
            Region("13", "Idaho", 200),
            Region("14", "Illinois", 200),
            Region("15", "Iowa", 200),
            Region("16", "Kansas", 200),
            Region("17", "Kentucky", 200),
            Region("18", "Lousiana", 200),
            Region("19", "Maine", 200),
            Region("20", "Maryland", 200),
            Region("21", "Massachusetts", 200),
            Region("22", "Michigan", 200),
            Region("23", "Minnesota", 200),
            Region("24", "Mississippi", 200),
            Region("25", "Missouri", 200),
            Region("26", "Montana", 200),
            Region("27", "Nebraska", 200),
            Region("28", "Nevada", 200),
            Region("29", "New Hamphire", 200),
            Region("30", "New Jersey", 200),
            Region("31", "New Mexico", 200),
            Region("32", "New York", 200),
            Region("33", "North Carolina", 200),
            Region("34", "North Dakota", 200),
            Region("35", "Ohio", 200),
            Region("36", "Oklahoma", 200),
            Region("37", "Oregon", 200),
            Region("38", "Pennsylvania", 200),
            Region("39", "Rhode Island", 200),
            Region("40", "South Carolina", 200),
            Region("41", "South Dakota", 200),
            Region("42", "Tennessee", 200),
            Region("43", "Texas", 200),
            Region("44", "Utah", 200),
            Region("45", "Vermont", 200),
            Region("46", "Virginia", 200),
            Region("47", "Washington", 200),
            Region("48", "West Virgina", 200),
            Region("49", "Wisconsin", 200),
            Region("50", "Wyoming", 200),
        ]

        self.__elections = [
            Election(
                "1", 
                "United States Presedential election, 2020", 
                self.__candidates[:2], 
                date(2020, 11, 3),
                self.__regions[:51],
                "Presidential"
            )
        ]

    def get_all_regions(self):
        return self.__regions

    def get_region_by_id(self, region_id: str) -> Region:
        for region in self.__regions:
            if region.id == region_id:
                return region
        return f"Region with id {region_id} was not found"

    def get_regions_by_election_id(self, election_id: str) -> list:
        for election in self.__elections:
            if election.id == election_id:
                return election.regions
        return f"Election with id {election_id} was not found"

    def create_region(self, region: RegionInputModel) -> Region:
        new_region = Region(str(self.__next_region_id), region.name, region.registered_voters)
        self.__next_region_id += 1
        self.__regions.append(new_region)
        return new_region
