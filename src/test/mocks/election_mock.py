from app.models.entities.election import Election
from app.models.entities.region import Region
from datetime import date
from app.repositories.database_context import DatabaseContext
from app.models.input_models.election_input_model import ElectionInputModel
from app.models.entities.candidate import Candidate

class ElectionMock:
    def __init__(self):
        self.__regions = [
            Region("1", "Alabama", 200),
            Region("2", "Alaska", 200),
            Region("3", "Arizona", 200),
            Region("4", "Arkansas", 200),
            Region("5", "California", 200),
            Region("6", "Colorado", 200),
            Region("7", "Connecticut", 200),
            Region("8", "Delaware", 200),
            Region("9", "D.C.", 200),
            Region("10", "Florida", 200),
            Region("11", "Georgia", 200),
            Region("12", "Hawaii", 200),
            Region("13", "Idaho", 200),
            Region("14", "Illinois", 200),
            Region("15", "Iowa", 200),
            Region("16", "Kansas", 200),
            Region("17", "Kentucky", 200),
            Region("18", "Lousiana", 200),
            Region("19", "Maine", 200),
            Region("20", "Maryland", 200),
            Region("21", "Massachusetts", 200),
            Region("22", "Michigan", 200),
            Region("23", "Minnesota", 200),
            Region("24", "Mississippi", 200),
            Region("25", "Missouri", 200),
            Region("26", "Montana", 200),
            Region("27", "Nebraska", 200),
            Region("28", "Nevada", 200),
            Region("29", "New Hamphire", 200),
            Region("30", "New Jersey", 200),
            Region("31", "New Mexico", 200),
            Region("32", "New York", 200),
            Region("33", "North Carolina", 200),
            Region("34", "North Dakota", 200),
            Region("35", "Ohio", 200),
            Region("36", "Oklahoma", 200),
            Region("37", "Oregon", 200),
            Region("38", "Pennsylvania", 200),
            Region("39", "Rhode Island", 200),
            Region("40", "South Carolina", 200),
            Region("41", "South Dakota", 200),
            Region("42", "Tennessee", 200),
            Region("43", "Texas", 200),
            Region("44", "Utah", 200),
            Region("45", "Vermont", 200),
            Region("46", "Virginia", 200),
            Region("47", "Washington", 200),
            Region("48", "West Virgina", 200),
            Region("49", "Wisconsin", 200),
            Region("50", "Wyoming", 200),
        ]
        self.__candidates = [
            Candidate(
            "1",
            "Donald J. Trump",
            date(1946, 6, 14),
            "Republican Party",
            "Currently the President of the United States"
        ),
        Candidate(
            "2",
            "Joe Biden",
            date(1942, 11, 20),
            "Democratic Party",
            "Former Vice President of the United States"
        )
        ]
        self.__elections = [
            Election("1", "United States Presidential election, 2020", self.__candidates[0:2], date(2020, 11, 3), DatabaseContext.regions, "Presidential"),
            Election("2", "United States Presidential election, 2024", self.__candidates[0:2], date(2024, 11, 6), DatabaseContext.regions, "Presidential")
        ]
        self.__next_election_id = len(self.__elections) + 1

    def get_all_elections(self) -> list:
        return self.__elections

    def get_election_by_id(self, id: str) -> Election:
        for election in self.__elections:
            if election.id == id:
                return election
        return f"Election with id {id} was not found"

    def get_region_by_id(self, id: str) -> Region:
        for region in self.__regions:
            if region.id == id:
                return region
        return f"Region with id {id} was not found"

    def get_candidate_by_id(self, id: str) -> Candidate:
        for candidate in self.__candidates:
            if candidate.id == id:
                return candidate
        return f"Candidate with id {id} was not found"

    def __get_entities_from_ids(self, ids: list, entity_type, get_entity_by_id) -> list:
        entities = []
        # Go through all the candidate ids and transfer them into models
        for id in ids:
            if isinstance(id, entity_type):
                return ids
            entity = get_entity_by_id(id)
            # Return an error string if we didn't find a candidate
            if isinstance(entity, str):
                return entity
            entities.append(entity)
        return entities

    def create_election(self, election: ElectionInputModel) -> Election:
        election.candidates = self.__get_entities_from_ids(election.candidates, Candidate, self.get_candidate_by_id)
        if isinstance(election.candidates, str):
            return election.candidates

        election.regions = self.__get_entities_from_ids(election.regions, Region, self.get_region_by_id)
        if isinstance(election.regions, str):
            return election.regions

        new_election = Election(str(self.__next_election_id), election.name, election.candidates, election.voting_date, election.regions, election.election_type)
        self.__elections.append(new_election)
        return new_election