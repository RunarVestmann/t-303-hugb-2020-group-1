from app.models.entities.candidate import Candidate
from app.models.entities.poll import Poll
from app.models.input_models.poll_input_model import PollInputModel
from app.models.entities.poll_data import PollData
from app.models.entities.region_poll import RegionPoll
from app.models.entities.region import Region
from app.models.entities.election import Election
from datetime import date

class PollMock:
    def __init__(self):
        self.__next_poll_id = 3
        self.__candidates = [
            Candidate("1", "Donald J. Trump", date(1946, 6, 14), "Republican Party", "Currently the president of the United States"),
            Candidate("2", "Joe Biden", date(1942, 11, 20), "Democratic party", "Former Vice President of the United States")
        ]
        
        self.__regions = [
            Region("1", "Alabama", 200),
            Region("2", "Alaska", 200),
            Region("3", "Arizona", 200),
            Region("4", "Arkansas", 200),
            Region("5", "California", 200),
            Region("6", "Colorado", 200),
            Region("7", "Connecticut", 200),
            Region("8", "Delaware", 200),
            Region("9", "D.C.", 200),
            Region("10", "Florida", 200),
            Region("11", "Georgia", 200),
            Region("12", "Hawaii", 200),
            Region("13", "Idaho", 200),
            Region("14", "Illinois", 200),
            Region("15", "Iowa", 200),
            Region("16", "Kansas", 200),
            Region("17", "Kentucky", 200),
            Region("18", "Lousiana", 200),
            Region("19", "Maine", 200),
            Region("20", "Maryland", 200),
            Region("21", "Massachusetts", 200),
            Region("22", "Michigan", 200),
            Region("23", "Minnesota", 200),
            Region("24", "Mississippi", 200),
            Region("25", "Missouri", 200),
            Region("26", "Montana", 200),
            Region("27", "Nebraska", 200),
            Region("28", "Nevada", 200),
            Region("29", "New Hamphire", 200),
            Region("30", "New Jersey", 200),
            Region("31", "New Mexico", 200),
            Region("32", "New York", 200),
            Region("33", "North Carolina", 200),
            Region("34", "North Dakota", 200),
            Region("35", "Ohio", 200),
            Region("36", "Oklahoma", 200),
            Region("37", "Oregon", 200),
            Region("38", "Pennsylvania", 200),
            Region("39", "Rhode Island", 200),
            Region("40", "South Carolina", 200),
            Region("41", "South Dakota", 200),
            Region("42", "Tennessee", 200),
            Region("43", "Texas", 200),
            Region("44", "Utah", 200),
            Region("45", "Vermont", 200),
            Region("46", "Virginia", 200),
            Region("47", "Washington", 200),
            Region("48", "West Virgina", 200),
            Region("49", "Wisconsin", 200),
            Region("50", "Wyoming", 200),
        ]
        self.__elections = [
            Election(
                "1", 
                "United States Presidential election, 2020", 
                self.__candidates[0:2],
                date(2020, 11, 3),
                self.__regions[:51],
                "Presidential"
            )
        ]

        self.__polls =  [
            Poll("1", "CNN",
            [
                RegionPoll([
                    PollData(self.__candidates[0], 33),
                    PollData(self.__candidates[1], 133),
                ], self.__regions[0]),

                RegionPoll([
                    PollData(self.__candidates[0], 22),
                    PollData(self.__candidates[1], 14),
                ], self.__regions[1]),

                RegionPoll([
                    PollData(self.__candidates[0], 78),
                    PollData(self.__candidates[1], 44),
                ], self.__regions[2])
            ], self.__elections[0]
            ),
            Poll("2", "NBC",
            [
                RegionPoll([
                    PollData(self.__candidates[0], 38),
                    PollData(self.__candidates[1], 88),
                ], self.__regions[8]),

                RegionPoll([
                    PollData(self.__candidates[0], 34),
                    PollData(self.__candidates[1], 18),
                ], self.__regions[5]),

                RegionPoll([
                    PollData(self.__candidates[0], 99),
                    PollData(self.__candidates[1], 78),
                ], self.__regions[34])
            ], self.__elections[0])
        ]

    def get_all_polls(self) -> list:
        return self.__polls

    def get_poll_by_id(self, id: str) -> Poll:
        for poll in self.__polls:
            if poll.id == id:
                return poll
        return f"Poll with id {id} was not found"

    def get_polls_by_election_id(self, election_id: str) -> list:
        election = self.__get_election_by_id(election_id)
        if isinstance(election, str):
            return election

        polls = []
        for poll in self.__polls:
            if poll.election.id == election_id:
                polls.append(poll)
        return polls

    def __get_election_by_id(self, id: str) -> Election:
        for election in self.__elections:
            if election.id == id:
                return election
        return f"Election with id {id} was not found"

    def get_poll_summary_by_election_id(self, election_id: str) -> list:
        election = self.__get_election_by_id(election_id)
        if isinstance(election, str):
            return election

        # We create a list with poll data for each candidate
        summary = []
        for candidate in election.candidates:
            summary.append(PollData(candidate, 0))

        polls = self.get_polls_by_election_id(election_id)
        for poll in polls:
            for region_poll in poll.region_polls:
                for poll_data in region_poll.data:
                    for item in summary:
                        # We add all the votes for each candidate to the summary
                        if item.candidate.id == poll_data.candidate.id:
                            item.votes += poll_data.votes           
        return summary, election


    def get_average_poll_by_election_id(self, election_id: str) -> Poll:
        
        polls = self.get_polls_by_election_id(election_id)
        if polls == None or len(polls) == 0: 
            f"No poll was found for election with id {election_id}"

        election = self.__get_election_by_id(election_id)
        if isinstance(election, str): 
            return election

        # Create Poll object with no id or conductor to hold all the poll info
        average_poll = Poll("", "", [], election)

        a_dict = {}

        for poll in polls:
            for region_poll in poll.region_polls:
                region_id = region_poll.region.id
                # We add each region poll to the newly created dict with the region id as key
                if region_id in a_dict:
                    a_dict[region_id].data.append(region_poll)
                else:
                    a_dict[region_id] = [region_poll]

        for region_id in a_dict:
            for region_poll in a_dict[region_id]:
                # We get the average votes for each region
                for poll_data in region_poll.data:
                    poll_data.votes = int(poll_data.votes / len(a_dict[region_id]))

            average_poll.region_polls.append(region_poll)

        return average_poll   

    def create_poll(self, *args) -> str:
        poll = args[0]

        if len(args) == 1:
            poll_election = None
            for election in self.__elections:
                if election.id == poll.election_id:
                    poll_election = election
                    break

            if poll_election == None:
                return f"Election with id {poll.election_id} was not found"

            valid_region_ids = [region.id for region in self.__regions]
            valid_candidate_ids = [candidate.id for candidate in self.__candidates]
            
            for region_poll in poll.region_polls:
                # Check whether the regions are valid
                if region_poll.region not in valid_region_ids:
                    return f"Region with id {region_poll.region} is not valid"
                region = [r for r in self.__regions if r.id == region_poll.region][0]
                region_poll.region = region
                data_list = []
                for data in region_poll.data:
                    # Check whether the candidates are valid
                    if data.candidate_id not in valid_candidate_ids:
                        return f"Candidate with id {data.candidate_id} is not valid"

                    data_list.append(PollData([c for c in self.__candidates if c.id == data.candidate_id][0], data.votes))
                region_poll.data = data_list  
        else:
            poll_election = args[1]

        new_poll = Poll(str(self.__next_poll_id), poll.conductor, poll.region_polls, poll_election)
        self.__polls.append(new_poll)
        return new_poll
