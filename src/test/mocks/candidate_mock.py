from app.models.entities.candidate import Candidate
from app.models.input_models.candidate_input_model import CandidateInputModel
from app.models.entities.election import Election
from datetime import date

class CandidateMock:
    def __init__(self):
        self.__next_candidate_id = 1
        self.__candidates = [
            Candidate("1", "Donald J. Trump", date(1946, 6, 14), "Republican Party", "Currently the president of the United States"),
            Candidate("2", "Joe Biden", date(1942, 11, 20), "Democratic party", "Former Vice President of the United States")
        ]
        self.__elections = [
            Election(
            "1", 
            "United States Presedential election, 2020", 
            [self.__candidates[0], self.__candidates[1]],
            date(2020, 11, 3),
            [],
            "Presidential"
            )
        ]

    def get_all_candidates(self) -> [Candidate]:
        return self.__candidates

    def get_candidate_by_id(self, candidate_id: str) -> Candidate:
        for candidate in self.__candidates:
            if candidate.id == candidate_id:
                return candidate
        return f"Candidate with id {candidate_id} was not found"

    def get_candidates_by_election_id(self, election_id: str) -> [Candidate]:
        for election in self.__elections:
            if election.id == election_id:
                return election.candidates
        return f"Election with id {election_id} was not found"

    def create_candidate(self, candidate: CandidateInputModel) -> Candidate:
        new_candidate = Candidate(
            str(self.__next_candidate_id), 
            candidate.name,
            candidate.birthdate,
            candidate.political_party,
            candidate.bio 
        )
        self.__next_candidate_id += 1
        self.__candidates.append(new_candidate)
        return new_candidate
