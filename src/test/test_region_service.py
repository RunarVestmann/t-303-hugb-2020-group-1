import unittest
from test.mocks.region_mock import RegionMock
from test.mocks.election_mock import ElectionMock
from app.services.region_service import RegionService
from app.models.input_models.region_input_model import RegionInputModel

class TestRegionService(unittest.TestCase):
    def setUp(self):
        self.__region_service = RegionService(RegionMock(), ElectionMock())

    def test_get_all_regions(self):
        regions = self.__region_service.get_all_regions()
        self.assertEqual(len(regions), 50)

    def test_get_region_by_id(self):
        region = self.__region_service.get_region_by_id("1")
        self.assertNotEqual(isinstance(region, str), True)
        self.assertEqual(region.id, "1")

    def test_get_regions_by_election_id(self):
        regions = self.__region_service.get_regions_by_election_id("1")
        self.assertEqual(len(regions), 50)

    def test_create_region(self):
        region = RegionInputModel("Georgia", 250)
        region_count = len(self.__region_service.get_all_regions())
        self.__region_service.create_region(region)
        self.assertEqual(len(self.__region_service.get_all_regions()), region_count+1)
