**A websocket server for election polls**

All of the libraries we are using can be found in src/requirements.txt

**If you don't mind globally installing those libraries then simply run**
```bash
pip install -r src/requirements.txt
```

**Otherwise if you'd like to install those libraries locally using a virtual enviroment**
```bash
cd src

pip install virtualenv

virtualenv -p python venv

Windows:                Mac/Linux:
venv\Scripts\activate   source venv/bin/activate

pip install -r requirements.txt
```
<br />

**Before you try and run any code it's important that you are located in the src folder**
```bash
cd src
```

<br />

Once you are in the src folder you can do one of the following commands:

**To run the server**
```bash
python app/server.py
```

**To run all unit tests**
```bash
python -m unittest discover test
```

**To run all unit tests and calculate coverage statistics**
```bash
coverage run --source app -m unittest discover test
```

**To see the calculated test coverage statistics you can run one of the following**
```bash
coverage report

coverage html
```
Due note that if you run the **coverage html** command you'll have to open up the newly created htmlcov folder and open the index.html in a browser to see the results
